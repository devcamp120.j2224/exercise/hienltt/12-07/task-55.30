package com.devcamp.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CDrinkController {
    @CrossOrigin
	@GetMapping("/drinks")
	public ArrayList<CDrink> getDrinkList(){
        ArrayList<CDrink> menu = new ArrayList<CDrink>();

        CDrink traTac = new CDrink("TRATAC", "Trà tắc");
        CDrink coca = new CDrink("COCA", "Cocacola");
        CDrink pepsi = new CDrink("PEPSI", "Pepsi");
        CDrink lavie = new CDrink("LAVIE", "Lavie");
        CDrink traSua = new CDrink("TRASUA", "Trà sữa trân châu");

        menu.add(traTac);
        menu.add(coca);
        menu.add(pepsi);
        menu.add(lavie);
        menu.add(traSua);

        return menu;
    }
}
