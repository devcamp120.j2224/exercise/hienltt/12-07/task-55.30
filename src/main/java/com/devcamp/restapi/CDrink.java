package com.devcamp.restapi;

public class CDrink {
    private String maNuocUong;
	public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    private String tenNuocUong; 

    public CDrink(String maNuocUong, String tenNuocUong){
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
    }
    @Override
	public String toString() {
		return "{maNuocUong=" + this.maNuocUong + ", tenNuocUong=" + this.tenNuocUong + "}";
	}
}
